// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import VueHighlightJS from 'vue-highlightjs'

import App from './App'

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueHighlightJS)

const store = new Vuex.Store({
  state: {
    url: window.location.hash
  },
  mutations: {
    changeUrl (state, url) {
      state.url = url
    }
  }
})

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  template: '<App/>',
  components: { App }
})

